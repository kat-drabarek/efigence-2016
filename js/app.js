$(document).foundation();

$(document).ready(function(){

// LOG IN PAGE:
	$("#submit-data").on("click", function(){
		var pass = $("#password-input").val();
		if (pass.length > 0){
			$("#validation-alert").addClass("invisible");
			postLoginData(pass);
		}
		else {
			displayAlert("Please enter your password.");
		};
	});

//TODO: podpiąć ajax do formularza:
	function postLoginData(pass){
		$.ajax({
			type: "post",
			data: {
				login: "efi",
				password: pass
			},
			url: "https://efigence-camp.herokuapp.com/api/login",
			error: function(response){
				var responseObject = JSON.parse(response.responseText);
				var errorAlert = responseObject.message;
				displayAlert(errorAlert);
			},
			success: function(response){
				window.location = "dashboard.html";
			}
		});
	}
	
	var alertTimer;
	function displayAlert(message){
		clearTimeout(alertTimer);
		$("#validation-alert").html(message).removeClass("invisible");
		alertTimer = setTimeout(function(){
			$("#validation-alert").addClass("invisible");
		}, 3000);
	}

// DASHBOARD PAGE:
	$(".logout").on("click", function(){
		window.location = "login-page.html";
	});

	$(".search").on("click", function(){
		$(".searchbar").slideToggle();
	});

	$(".close-searchbar").on("click", function(){
		$(".searchbar").slideToggle();
	});

	function appendSummary(){
		$.ajax({
			type: "get",
			url: "https://efigence-camp.herokuapp.com/api/data/summary",
			success: function(response){
				$(".summary.balance").text(response.content[0].balance + " PLN");				
				$(".summary.funds").text(response.content[0].funds + " PLN");
				$(".summary.payments").text(response.content[0].payments + " PLN");
			}
		});
	}
	appendSummary();

	function appendProducts(){
		$.ajax({
			type: "get",
			url: "https://efigence-camp.herokuapp.com/api/data/products",
			success: function(response){
				$(".products.wallets").text(response.content[0].amount + " " + response.content[0].currency);
				$(".products.deposits").text(response.content[1].amount + " " + response.content[1].currency);
				$(".products.accounts").text(response.content[2].amount + " " + response.content[2].currency);
				$(".products.funds").text(response.content[3].amount + " " + response.content[3].currency);
				$(".products.loans").text(response.content[4].amount + " " + response.content[4].currency);
			}
		});
	}
	appendProducts();

	// $(function () {
	// 	$('#container').highcharts({
	// 		chart: {
	// 			type: 'area'
	// 		},
	// 		title: {
	// 			text: ''
	// 		},
	// 		xAxis: {
	// 			categories: []
	// 		},
	// 		credits: {
	// 			enabled: false
	// 		},
	// 		series: [{
	// 			name: '',
	// 		data: [5, 3, 4, 0, -2]
	// 		}]
	// 	});
	// });

new Chartist.Line('.ct-chart', {
  labels: [1, 2, 3, 4, 5, 6, 7, 8],
  series: [
    [1, 2, 3, 1, -2, 0, 1, 0]
  ]
}, {
  high: 3,
  low: -3,
  showArea: true,
  showLine: false,
  showPoint: false,
  fullWidth: true,
  axisX: {
    showLabel: false,
    showGrid: false
  }
});

	function appendHistory(){
		$.ajax({
			type: "get",
			url: "https://efigence-camp.herokuapp.com/api/data/history",
			success: function(response){
			}
		});
	}
	appendHistory();

	function appendChartData(){
		$.ajax({
			type: "get",
			url: "https://efigence-camp.herokuapp.com/api/data/history",
			success: function(response){
				var chartAmounts = [];
				for(var i = 0; i < 10; i++){
					chartAmounts.push(response.content[i].amount);
				};
				console.log(chartAmounts);
				console.log(response);
			}
		});
	}
	appendChartData();

// BUDGETS PAGE:
	$(".diagram").on("click", function(){
		$(".diagram").removeClass("active-diagram");
		$(this).addClass("active-diagram");
	});
	$(".diagram-close").on("click", function(e){
		$(this).parent().removeClass("active-diagram");
		e.stopPropagation();
	});
});
